package com.blogger.genjs.concurrency.iii;

import static org.junit.Assert.assertEquals;

public class FindBestWorkerTest {

	@org.junit.Test
	public void test() throws InterruptedException {

		Thread thread = new Thread(new Runnable() {
			public void run() {
				new Dispatcher().runWorkers(new Job());
			}
		});
		thread.start();

		Thread.sleep(1000);

		assertEquals(Thread.State.WAITING, thread.getState());
	}
}