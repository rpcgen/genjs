package com.blogger.genjs.concurrency.iii;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

class Dispatcher {

	private Worker[] workers = new Worker[] {
			new Worker(),
			new Worker(),
			new Worker()
	};

	private Map<Job, Result> results = new HashMap<Job, Result>();

	synchronized void finish(Job job, Result result) {
		if (results.get(job) == null) {
			results.put(job, result);
		}
	}

	synchronized Result runWorkers(final Job job) {

		results.put(job, null);

		Thread[] threads = new Thread[workers.length];

		for (final Worker worker : workers) {
			threads[Arrays.asList(workers).indexOf(worker)] =
				new Thread(new Runnable() {
					public void run() {
						worker.work(Dispatcher.this, job);
					}
				});
		}

		for (Thread thread : threads) {
			thread.start();
		}

		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return results.get(job);
	}
}