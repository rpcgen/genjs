package com.blogger.genjs.concurrency.iii;

class Worker {

	void work(final Dispatcher dispatcher, final Job job) {
		dispatcher.finish(job, new Result());
	}
}