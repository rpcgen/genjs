package com.blogger.genjs.concurrency.ii;

enum ForkRequest {
	FIRST_FORK, SECOND_FORK;
}
