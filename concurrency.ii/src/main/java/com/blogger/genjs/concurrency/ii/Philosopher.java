package com.blogger.genjs.concurrency.ii;

import java.util.Random;

class Philosopher implements Runnable {

	private static Random generator = new Random();

	private Table table;

	Philosopher(Table table) {
		this.table = table;
	}

	private void doWait() {
		try {
			Thread.sleep(generator.nextInt(50));
		} catch (InterruptedException exception) {
			exception.printStackTrace();
		}
	}

	private void doEat(Fork _1, Fork _2) {
		doWait();
	}

	private void think() {
		doWait();
	}

	private void eat() {

		Fork fork1 = table.getFork(this, ForkRequest.FIRST_FORK );
		Fork fork2 = table.getFork(this, ForkRequest.SECOND_FORK);

		synchronized (fork1) {
			synchronized (fork2) {
				doEat(fork1, fork2);
			}
		}
	}

	public void run() {
		for (int i = 0; i < 100; i++) {
			think();
			eat();
		}
	}
}
