package com.blogger.genjs.concurrency.ii;

import java.util.Arrays;

class Table implements Runnable {

	private Philosopher[] philosophers;
	private Fork[] forks;
	private Fork[][] rel;

	Table(int x) {

		philosophers = new Philosopher[x];
		forks = new Fork[x];
		rel = new Fork[x][2];

		for (int i = 0; i < x; i++) {
			philosophers[i] = new Philosopher(this);
			forks[i] = new Fork();
		}

		for (int i = 0; i < x; i++) {
			rel[i][0] = forks[i];
			rel[i][1] = forks[(i + 1 == x) ? 0 : i + 1];
		}
	}

	Fork getFork(Philosopher philosopher, ForkRequest request) {

		int p  = Arrays.asList(philosophers).indexOf(philosopher);
		int f1 = Arrays.asList(forks).indexOf(rel[p][0]);
		int f2 = Arrays.asList(forks).indexOf(rel[p][1]);

		switch (request) {
		case FIRST_FORK:
			return f1 < f2 ? forks[f1] : forks[f2];
		case SECOND_FORK:
			return f1 < f2 ? forks[f2] : forks[f1];
		}

		return null;
	}

	public void run() {

		Thread[] threads = new Thread[philosophers.length];

		for (int i = 0; i < philosophers.length; i++) {
			threads[i] = new Thread(
					philosophers[i],
					"Philosopher-" + i);
			threads[i].start();
		}

		for (Thread thread : threads) {
			try {
				thread.join();
			} catch (InterruptedException exception) {
				exception.printStackTrace();
			}
		}
	}
}
