package com.blogger.genjs.concurrency.i;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.lang.Thread.UncaughtExceptionHandler;

import com.blogger.genjs.concurrency.i.ThreadSafe1;
import com.blogger.genjs.concurrency.i.ThreadSafe2;
import com.blogger.genjs.concurrency.i.ThreadUnsafe;

public class SynchronizedMethodsTest {

	public static void runTest(
			final ThreadUnsafe             obj,
			final UncaughtExceptionHandler handler) {

		Thread t1 = new Thread(new Runnable() {
			public void run() {
				for (int i = 0; i < 50; i++) {
					obj.action1();
					obj.action2();
				}
			}
		});

		Thread t2 = new Thread(new Runnable() {
			public void run() {
				for (int i = 0; i < 50; i++) {
					obj.action1();
					obj.action2();
				}
			}
		});

		if (handler != null) {
			t1.setUncaughtExceptionHandler(handler);
			t2.setUncaughtExceptionHandler(handler);
		}

		t1.start();
		t2.start();

		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@org.junit.Test
	public final void test1() {
		runTest(new ThreadSafe1(), null);
	}
	@org.junit.Test
	public final void test2() {
		runTest(new ThreadSafe2(), null);
	}
	@org.junit.Test
	public final void testError() {

		UncaughtExceptionHandler handler =
				mock(UncaughtExceptionHandler.class);

		runTest(new ThreadUnsafe(), handler);

		verify(handler, atLeastOnce()).uncaughtException(
				any(Thread.class),
				any(Throwable.class));
	}
}

