package com.blogger.genjs.concurrency.i;

class ThreadSafe1 extends ThreadUnsafe {

	private Object _1 = new Object();
	private Object _2 = new Object();

	void action1() {
		synchronized (_1) { super.action1(); }
	}
	void action2() {
		synchronized (_2) { super.action2(); }
	}
}