package com.blogger.genjs.concurrency.i;

class ThreadUnsafe {

	public final long ACTION_TIME = 5;
	private int _1 = 0;
	private int _2 = 0;

	private void error() {
		throw new RuntimeException("Illegal state.");
	}
	private void sleep() {
		try {
			Thread.sleep(ACTION_TIME);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	void action1() { if (++_1 > 1) error(); else sleep(); --_1;}
	void action2() { if (++_2 > 1) error(); else sleep(); --_2;}
}