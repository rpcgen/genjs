package com.blogger.genjs.concurrency.i;

class ThreadSafe2 extends ThreadUnsafe {

	synchronized void action1() {
		super.action1();
	}
	synchronized void action2() {
		super.action2();
	}
}